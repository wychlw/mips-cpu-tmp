
# <center>NYAN MIPS</center>

> :warning: **<a style="color:yellow;">Warning</a>**<br/>
If you find messy code in Chinese, please check if you are using GBK or UTF-8 encoding. If you are using GBK, please change it to UTF-8.

[TOC]

## Introduction

This is a tiny MIPS cpu core, which implement some basic mathmatic and jump ISAs (see [support_inst](./support_inst.md) for more info).

The structure of the cpu is a three-stage pipeline: cu, exu, memu.

This CPU is not very fast, but writing it for a clear and understandable example of the pipline tech.

## Code Structure

The srcs is in the `vsrc` foder.

*Anything not showing below is not used.*
```
vsrc
|
|---top.v # top
|
|---mmu.v
|
|---bus.v # connect the inst bus and data bus together
|
|---io # input and output
|   |
|   |---iommu.v # connect to mmu
|   |
|   |---uart_rx.v
|   |
|   |---uart_tx.v
|
|---core # where do all the calculation
|   |
|   |---core_top.v
|   |
|   |---npc.v
|   |
|   |---reg.v
|   |
|   |---reg_forward.v # forward the reg in pipline
|   |
|   |---cu.v
|   |
|   |---exu.v
|   |
|   |---memu.v
|
|---cal # like adder unit, shifter unit... all these kind of stuff.
|   |
|   |---comp # sub component
|   |   |
|   |   |---cla.v
|   |   |
|   |   |---bcla.v
|   |   |
|   |   |---mbcla.v # connect two bcla together
|   |
|   |---adder32.v
|   |
|   |---lshifter.v
|   |
|   |---rshifter.v
|
|---util
|   |
|   |---mux.v
|   |
|   |---dreg.v
|   |
|   |---encoder.v
|   |
|   |---decoder.v
|   |
|   |---sr_reg.v # A register which can be set and reset(mainly a wrapper)
|   |
|   |---shift_reg.v
|   |
|   |---circulate_reg.v # circulate a given init value inside the register, mainly count the stage.
|   |
|   |---counter_timer.v # Set to 1 after a delay, also a wrapper.
|   |
|   |---delay_reg.v # de;ay the signal for a given time.


```

## CPU structure
The CPU is a three-stage pipline CPU. It uses handshake to sync each unit.

It is slow, indeed. You can detach the mem access clock and core clock, to make it run faster. Though you may need to change the 2-stage handshake to 3-stage handshake.

## Usage

You may want to create your own constr.

If you want to do post-synthesis simulation only, you can use verilator to do it.

For bitstrame generation, the verison I use is vivado2023. If you use a lower version, you may need to replace the clock ip.

## Mem mapping

The 0x80000000 is the reset address of the vaddr, which maps to the 0x00000000 of the paddr. In the platform I use, the 0x80000000 - 0x8003FFFFF is the first SRAM and 0x800400000 - 0x8008FFFF is the second SRAM. The UART STAT addr is 0xbfd003fc, the UART DATA addr is 0xbfd003f8.

**Change the mmu and iommu unit based on your need!**

## Notice

This CPU is previously create for *NSCSCC* compition. Though I didn't use any of the example code, the input and output is still based on that specific platform (like ram access and uart). You may need to cange some timing if any weird thing happens.

## TODO
[] Multiply inst(V6 ver.)

[] Core inst cache
[] Core mem cache

[] L2 cache

[] Decopule mem timing and core timing
- RAM clock: 50MHZ
- CORE clock: 100MHZ

## 