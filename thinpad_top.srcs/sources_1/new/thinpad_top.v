`default_nettype none

module thinpad_top(
    input wire clk_50M,           //50MHz 时钟输入
    input wire clk_11M0592,       //11.0592MHz 时钟输入（备用，可不用）

    input wire clock_btn,         //BTN5手动时钟按钮开关，带消抖电路，按下时为1
    input wire reset_btn,         //BTN6手动复位按钮开关，带消抖电路，按下时为1

    input  wire[3:0]  touch_btn,  //BTN1~BTN4，按钮开关，按下时为1
    input  wire[31:0] dip_sw,     //32位拨码开关，拨到“ON”时为1
    output wire[15:0] leds,       //16位LED，输出时1点亮
    output wire[7:0]  dpy0,       //数码管低位信号，包括小数点，输出1点亮
    output wire[7:0]  dpy1,       //数码管高位信号，包括小数点，输出1点亮

    //BaseRAM信号
    inout wire[31:0] base_ram_data,  //BaseRAM数据，低8位与CPLD串口控制器共享
    output wire[19:0] base_ram_addr, //BaseRAM地址
    output wire[3:0] base_ram_be_n,  //BaseRAM字节使能，低有效。如果不使用字节使能，请保持为0
    output wire base_ram_ce_n,       //BaseRAM片选，低有效
    output wire base_ram_oe_n,       //BaseRAM读使能，低有效
    output wire base_ram_we_n,       //BaseRAM写使能，低有效

    //ExtRAM信号
    inout wire[31:0] ext_ram_data,  //ExtRAM数据
    output wire[19:0] ext_ram_addr, //ExtRAM地址
    output wire[3:0] ext_ram_be_n,  //ExtRAM字节使能，低有效。如果不使用字节使能，请保持为0
    output wire ext_ram_ce_n,       //ExtRAM片选，低有效
    output wire ext_ram_oe_n,       //ExtRAM读使能，低有效
    output wire ext_ram_we_n,       //ExtRAM写使能，低有效

    //直连串口信号
    output wire txd,  //直连串口发送端
    input  wire rxd,  //直连串口接收端

    //Flash存储器信号，参考 JS28F640 芯片手册
    output wire [22:0]flash_a,      //Flash地址，a0仅在8bit模式有效，16bit模式无意义
    inout  wire [15:0]flash_d,      //Flash数据
    output wire flash_rp_n,         //Flash复位信号，低有效
    output wire flash_vpen,         //Flash写保护信号，低电平时不能擦除、烧写
    output wire flash_ce_n,         //Flash片选信号，低有效
    output wire flash_oe_n,         //Flash读使能信号，低有效
    output wire flash_we_n,         //Flash写使能信号，低有效
    output wire flash_byte_n,       //Flash 8bit模式选择，低有效。在使用flash的16位模式时请设为1

    //图像输出信号
    output wire[2:0] video_red,    //红色像素，3位
    output wire[2:0] video_green,  //绿色像素，3位
    output wire[1:0] video_blue,   //蓝色像素，2位
    output wire video_hsync,       //行同步（水平同步）信号
    output wire video_vsync,       //场同步（垂直同步）信号
    output wire video_clk,         //像素时钟输出
    output wire video_de           //行数据有效信号，用于区分消隐区
);

wire locked, clk_20M, clk_50M_out, clk_100M, clk_75M;
pll_example clock_gen 
 (
  .clk_in1(clk_50M),
  .clk_out1(clk_20M),
  .clk_out2(clk_50M_out),
  .clk_out3(clk_100M),
  .clk_out4(clk_75M),
  .reset(reset_btn),
  .locked(locked)
 );

wire core_clk = clk_75M;
wire mem_clk = clk_75M;
wire rst_use = reset_btn | ~locked;

wire inst_req;
wire inst_res;
wire[31:0] inst_addr;
wire[31:0] fetch_inst;
wire bus_req;
wire bus_res;
wire bus_rw;
wire[31:0] bus_data_in;
wire[31:0] bus_data_out;
wire[31:0] bus_addr;
wire[1:0] len;

core_top core_top_comp(
    .clk(core_clk),
    .rst(rst_use),
    .inst_req(inst_req),
    .inst_res(inst_res),
    .inst_addr(inst_addr),
    .fetch_inst(fetch_inst),
    .bus_req(bus_req),
    .bus_res(bus_res),
    .bus_rw(bus_rw),
    .bus_data_in(bus_data_in),
    .bus_data_out(bus_data_out),
    .bus_addr(bus_addr),
    .len(len)
);

wire[31:0] mmu_addr;
wire[31:0] mmu_data_in;
wire[31:0] mmu_data_out;
wire mmu_rw;
wire[1:0] mmu_wlen;
wire mmu_req;
wire mmu_res;

bus bus_comp(
    .clk(mem_clk),
    .rst(rst_use),
    
    .inst_req(inst_req),
    .inst_res(inst_res),
    .inst_addr(inst_addr),
    .fetch_inst(fetch_inst),
    .bus_req(bus_req),
    .bus_res(bus_res),
    .bus_rw(bus_rw),
    .bus_addr(bus_addr),
    .bus_data_in(bus_data_in),
    .bus_data_out(bus_data_out),
    .len(len),

    .addr(mmu_addr),
    .data_in(mmu_data_out),
    .data_out(mmu_data_in),
    .rw(mmu_rw),
    .wlen(mmu_wlen),
    .req(mmu_req),
    .res(mmu_res)
);

wire[31:0] iommu_addr;
wire[31:0] iommu_data_in;
wire[31:0] iommu_data_out;
wire iommu_rw;
wire[3:0] iommu_be;
wire iommu_req;
wire iommu_res;

mmu mmu_comp(
    .clk(mem_clk),
    .rst(rst_use),
    
    .addr(mmu_addr),
    .data_in(mmu_data_in),
    .data_out(mmu_data_out),
    .rw(mmu_rw),
    .wlen(mmu_wlen),
    .req(mmu_req),
    .res(mmu_res),

    .base_ram_addr(base_ram_addr),
    .base_ram_data(base_ram_data),
    .base_ram_be_n(base_ram_be_n),
    .base_ram_ce_n(base_ram_ce_n),
    .base_ram_oe_n(base_ram_oe_n),
    .base_ram_we_n(base_ram_we_n),

    .ext_ram_addr(ext_ram_addr),
    .ext_ram_data(ext_ram_data),
    .ext_ram_be_n(ext_ram_be_n),
    .ext_ram_ce_n(ext_ram_ce_n),
    .ext_ram_oe_n(ext_ram_oe_n),
    .ext_ram_we_n(ext_ram_we_n),

    .iommu_addr(iommu_addr),
    .iommu_be(iommu_be),
    .iommu_data_in(iommu_data_out),
    .iommu_data_out(iommu_data_in),
    .iommu_rw(iommu_rw),
    .iommu_req(iommu_req),
    .iommu_res(iommu_res)
);

wire uart_rx_rdy;
wire uart_rx_clr;
wire[7:0] uart_rx_data;
wire uart_tx_start;
wire uart_tx_busy;
wire[7:0] uart_tx_data;

iommu iommu_comp(
    .clk(mem_clk),
    .rst(rst_use),

    .addr(iommu_addr),
    .data_in(iommu_data_in),
    .data_out(iommu_data_out),
    .rw(iommu_rw),
    .be(iommu_be),
    .req(iommu_req),
    .res(iommu_res),
    
    .rx_rdy(uart_rx_rdy),
    .rx_clr(uart_rx_clr),
    .rx_data(uart_rx_data),
    .tx_start(uart_tx_start),
    .tx_busy(uart_tx_busy),
    .tx_data(uart_tx_data)
);

// async_transmitter uart_tx_comp(
//     .clk(mem_clk),
//     .TxD(txd),
//     .TxD_start(uart_tx_start),
//     .TxD_data(uart_tx_data),
//     .TxD_busy(uart_tx_busy)
// );

async_receiver uart_rx_comp(
    .clk(mem_clk),
    .RxD(rxd),
    .RxD_data_ready(uart_rx_rdy),
    .RxD_clear(uart_rx_clr),
    .RxD_data(uart_rx_data)
);

uart_tx uart_tx_comp(
    .clk(mem_clk),
    .rst(rst_use),
    .txd(txd),
    .baud_clk(clk_11M0592),
    .tx_data_in(uart_tx_data),
    .tx_start(uart_tx_start),
    .tx_busy(uart_tx_busy)
);

// uart_rx uart_rx_comp(
//     .clk(clk_use),
//     .rst(rst_use),
//     .rxd(rxd),
//     .baud_clk(clk_11M0592),
//     .rx_data_out(uart_rx_data),
//     .rx_rst(uart_rx_clr),
//     .rx_ready(uart_rx_rdy)
// );

endmodule
