module regs (
    input wire clk,
    input wire rst,
    input wire[4:0] s,
    input wire[4:0] r,
    input wire[4:0] w,
    input wire w_en,
    input wire[31:0] w_d,
    output wire[31:0] s_d,
    output wire[31:0] r_d
);

wire[30:0] w_onehot_t;
wire w_onehot_t_unused;

decoder#(5) w_decoder(
    .en(1'b1),
    .x(w),
    .y({w_onehot_t,w_onehot_t_unused})
);

wire[31:0] w_onehot;
assign w_onehot = {w_onehot_t[30:0], 1'b0};     // the r0 should always be 0.

wire[31:0] reg_out[31:0];

genvar i;
generate
    for (i = 0; i < 32; i = i + 1) begin
        dreg#(32, 32'b0) regi(
            .clk(clk),
            .rst(rst),
            .en(w_onehot[i] & w_en),
            .din(w_d),
            .dout(reg_out[i])
        );
    end
endgenerate

wire[32 * (5 + 32) - 1 : 0] luts;
generate
    for (i = 0; i < 32; i = i + 1)
    begin
        assign luts[(5 + 32) * (i + 1) - 1 : (5 + 32) * i] = {
            i[4:0], reg_out[i]
        };
    end
endgenerate

mux#(32, 5, 32) s_mux(
    .key(s),
    .default_o(32'b0),
    .out(s_d),
    .lut(luts)
);

mux#(32, 5, 32) r_mux(
    .key(r),
    .default_o(32'b0),
    .out(r_d),
    .lut(luts)
);

endmodule
