module npc (
    input wire clk,
    input wire rst,

    // to ex
    input wire ex_req,
    input wire flag_in,
    input wire[31:0] jaddr_in,

    // to pc
    input wire cu_req,
    input wire jmp,
    output wire[31:0] pc,
    output wire pc_ready
);

wire[31:0] pc_now;
wire[31:0] pc_nxt;

assign pc = pc_now;

wire flag;
wire[31:0] jaddr;

dreg#(1, 1'b0) flag_reg(
    .clk(clk),
    .rst(rst),
    .en(ex_req),
    .din(flag_in),
    .dout(flag)
);
dreg#(32, 32'b0) jaddr_reg(
    .clk(clk),
    .rst(rst),
    .en(ex_req),
    .din(jaddr_in),
    .dout(jaddr)
);

wire npc_run;
assign npc_run = ex_req;

dreg#(32, 32'h80000000) pc_reg(
    .clk(clk),
    .rst(rst),
    .en(pc_ready),
    .din(pc_nxt),
    .dout(pc_now)
);

// config delay slot
wire in_delay;
dreg#(1, 1'b0) delay_reg(
    .clk(clk),
    .rst(rst | (flag & in_delay)),
    .en(cu_req),
    .din(jmp),
    .dout(in_delay)
);

// generate npc

// generate pc+4
wire co_unused, of_unused;
wire[31:0] pc_p4;
adder32 pc_p4_adder(
    .a(pc_now),
    .b(32'h4),
    .ci(1'b0),
    .s(pc_p4),
    .co(co_unused),
    .of(of_unused)
);

// switch from pc+4 and jaddr
assign pc_nxt = (flag & in_delay) ? jaddr : pc_p4; 

// post pc_ready (actually delay the pc_req for one clk)
dreg#(1, 1'b1) ready_reg(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(npc_run),
    .dout(pc_ready)
);

endmodule
