module reg_forward (
    // ex forward
    input wire[4:0] ex_wbaddr,
    input wire[31:0] ex_out,
    input wire mem_req,
    input wire ex_stage0,

    // mem forward
    input wire[4:0] mem_wbaddr,
    input wire[31:0] mem_out,
    input wire memwb_en,
    input wire mem_stage0,

    // reg forward
    output wire[4:0] regs_reg0_addr,
    input wire[31:0] regs_reg0,
    output wire[4:0] regs_reg1_addr,
    input wire[31:0] regs_reg1,

    // to cu
    input wire[4:0] reg0_addr,
    output wire[31:0] reg0,
    output wire reg0_valid,
    input wire[4:0] reg1_addr,
    output wire[31:0] reg1,
    output wire reg1_valid
);

assign regs_reg0_addr = reg0_addr;
assign regs_reg1_addr = reg1_addr;

// reg0
wire reg0_zero = ~|reg0_addr;

wire reg0_ex_forward = &(reg0_addr ~^ ex_wbaddr) & ~ex_stage0;
wire reg0_ex_valid = mem_req;

wire reg0_mem_forward = &(reg0_addr ~^ mem_wbaddr) & ~mem_stage0;
wire reg0_mem_valid = memwb_en;

assign reg0 = reg0_zero ? regs_reg0 :
              reg0_ex_forward ? ex_out :
              reg0_mem_forward ? mem_out :
              regs_reg0;
assign reg0_valid = reg0_zero ? 1'b1 :
                    reg0_ex_forward ? reg0_ex_valid :
                    reg0_mem_forward ? reg0_mem_valid :
                    1'b1;

// reg1
wire reg1_zero = ~|reg1_addr;

wire reg1_ex_forward = &(reg1_addr ~^ ex_wbaddr) & ~ex_stage0;
wire reg1_ex_valid = mem_req;

wire reg1_mem_forward = &(reg1_addr ~^ mem_wbaddr) & ~mem_stage0;
wire reg1_mem_valid = memwb_en;

assign reg1 = reg1_zero ? regs_reg1 :
              reg1_ex_forward ? ex_out :
              reg1_mem_forward ? mem_out :
              regs_reg1;
assign reg1_valid = reg1_zero ? 1'b1 : 
                    reg1_ex_forward ? reg1_ex_valid :
                    reg1_mem_forward ? reg1_mem_valid :
                    1'b1;
    
endmodule
