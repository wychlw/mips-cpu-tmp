module exu (
    input wire clk,
    input wire rst,

    // to cu
    input wire ex_req,
    input wire[31:0] data0_in,
    input wire[31:0] data1_in,
    input wire[31:0] inst_in,
    input wire[31:0] pc_in,
    output wire ex_res,
    input wire[31:0] cmp_data0_in,
    input wire[31:0] cmp_data1_in,

    // to npc
    output wire npc_req,
    output wire flag,
    output wire[31:0] jaddr,

    // to mem
    output wire mem_req,
    output wire[31:0] data_out,
    input wire mem_res,

    // to pipe
    output wire stage0
);

wire[31:0] data0;
dreg#(32, 32'b0) data0_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(data0_in),
    .dout(data0)
);

wire[31:0] data1;
dreg#(32, 32'b0) data1_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(data1_in),
    .dout(data1)
);

wire[31:0] inst;
dreg#(32, 32'b0) inst_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(inst_in),
    .dout(inst)
);

wire[31:0] pc;
dreg#(32, 32'b0) pc_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(pc_in),
    .dout(pc)
);

wire[31:0] cmp_data0;
dreg#(32, 32'b0) cmp_data0_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(cmp_data0_in),
    .dout(cmp_data0)
);

wire[31:0] cmp_data1;
dreg#(32, 32'b0) cmp_data1_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(cmp_data1_in),
    .dout(cmp_data1)
);

// first part, calculate all results.
wire[31:0] add_out;
wire[31:0] and_out;
wire[31:0] or_out;
wire[31:0] nor_out;
wire[31:0] xor_out;
wire[31:0] sll_out;
wire[31:0] srl_out;
wire[31:0] sra_out;
wire[31:0] slt_out;
wire[31:0] sltu_out;
wire[31:0] mul_out;

// second part, cal all flags
wire eq, ne, eqz, gtz, gez, ltz, lez;

// third part, select the flag and do all the timing
/**
 * stages:
 *      stage0: recive mem_res; waiting for ex_req
 *      stage1: recive ex_req; send ex_res, set counter to wait the cal
 *      stage2: ;
 *      stage3: counter end; send mem_req, send npc_req
 *      stage4: ; reset npc_req
 *
 * notice: when counter has set, one clk is passed. so set to cyc-1 
 */
wire[4:0] stage;
wire nxt_stage;
wire[3:0] cyc;  // change on demand
wire counter_fin;
//00000000000000000000000000000000
assign ex_res = stage[1];
assign mem_req = stage[3] | stage[4];
assign npc_req = stage[3];

assign nxt_stage = (stage[0] & ex_req) |
                   (stage[1]) |
                   (stage[2] & counter_fin) | 
                   (stage[3]) |
                   (stage[4] & mem_res);

assign jaddr = add_out;
assign stage0 = stage[0];

circulate_reg#(5, 5'b0001) stage_reg(
    .clk(clk),
    .rst(rst),
    .en(nxt_stage),
    .dout(stage)
);
counter_timer#(4) counter_timer(
    .clk(clk),
    .rst(rst),
    .cyc(cyc),
    .start(stage[1]),
    .fin(counter_fin)
);

wire[5:0] op_code;
assign op_code = inst[31:26];
wire[5:0] funt_code;
assign funt_code = inst[5:0];
wire[4:0] imm_code;
assign imm_code = inst[20:16];

// switch for flag

// switch for data out

// first part impl
wire is_sub;
wire is_sub_funt;
mux#(4, 6, 1) is_sub_funt_mux(
    .key(funt_code),
    .default_o(1'b0),
    .out(is_sub_funt),
    .lut(
        {
            6'b100010, 1'b1, // sub
            6'b100011, 1'b1, // subu
            6'b101010, 1'b1, // slt
            6'b101011, 1'b1 // sltu
        }
    )
);
mux#(3, 6, 1) is_sub_op_mux(
    .key(op_code),
    .default_o(1'b0),
    .out(is_sub),
    .lut(
        {
            6'b000000, is_sub_funt, // special
            6'b001010, 1'b1, // slti
            6'b001011, 1'b1 //sltiu
        }
    )
);
wire adder_co;
wire adder_of;
adder32 adder(
    .a(data0),
    .b(is_sub ? ~data1 : data1),
    .ci(is_sub),
    .s(add_out),
    .co(adder_co),
    .of(adder_of)
);
// mul32 muler(
//     .a(data0),
//     .b(data1),
//     .out(mul_out)
// );
assign mul_out = $signed(data0) * $signed(data1);
assign and_out = data0 & data1;
assign or_out = data0 | data1;
assign nor_out = ~(data0 | data1);
assign xor_out = data0 ^ data1;
lshifter#(32) sll_shifter(
    .a(data0),
    .b(data1[4:0]),
    .o(sll_out)
);
rshifter#(32, 1) srl_shifter(
    .a(data0),
    .b(data1[4:0]),
    .o(srl_out)
);
rshifter#(32, 0) sra_shifter(
    .a(data0),
    .b(data1[4:0]),
    .o(sra_out)
);
assign slt_out = {31'b0, add_out[31] ^ adder_of};
assign sltu_out = {31'b0, is_sub ^ adder_co};

// second part impl
assign eq = &(cmp_data0 ~^ cmp_data1);
assign ne = ~eq;
assign eqz = ~(|cmp_data0);
assign gez = ~cmp_data0[31];
assign gtz = gez & ~eqz;
assign ltz = cmp_data0[31];
assign lez = ltz | eqz;

wire[31:0] pcp8;
assign pcp8 = pc + 8;

// now begin mux to choose the output
wire[31:0] special_data;
wire special_flag;
wire[3:0] special_cyc;
mux#(18, 6, 37) special_mux(
    .key(funt_code),
    .default_o(37'b0),
    .out({special_data, special_flag, special_cyc}),
    .lut(
        {
            6'b100000, add_out, 1'b0, 4'd0,
            // add
            6'b100001, add_out, 1'b0, 4'd0,
            // addu
            6'b100100, and_out, 1'b0, 4'd0,
            // and
            6'b001000, add_out, 1'b1, 4'd0,
            // jr
            6'b001001, pcp8, 1'b1, 4'd0,
            // jalr
            6'b100111, nor_out, 1'b0, 4'd0,
            // nor
            6'b100101, or_out, 1'b0, 4'd0,
            // or
            6'b000000, sll_out, 1'b0, 4'd0,
            // sll
            6'b000100, sll_out, 1'b0, 4'd0,
            // sllv
            6'b101010, slt_out, 1'b0, 4'd0,
            // slt
            6'b101011, sltu_out, 1'b0, 4'd0,
            // sltu
            6'b000011, sra_out, 1'b0, 4'd0,
            // sra
            6'b000111, sra_out, 1'b0, 4'd0,
            // srav
            6'b000010, srl_out, 1'b0, 4'd0,
            // srl
            6'b000110, srl_out, 1'b0, 4'd0,
            // srlv
            6'b100010, add_out, 1'b0, 4'd0,
            // sub
            6'b100011, add_out, 1'b0, 4'd0,
            // subu
            6'b100110, xor_out, 1'b0, 4'd0
            // xor
        }
    )
);

wire[31:0] regimm_data;
wire regimm_flag;
wire[3:0] regimm_cyc;
mux#(2, 5, 37) regimm_mux(
    .key(imm_code),
    .default_o(37'b0),
    .out({regimm_data, regimm_flag, regimm_cyc}),
    .lut(
        {
            5'b00001, add_out, gez, 4'd0,
            // bgez
            5'b00000, add_out, ltz, 4'd0
            // bltz
        }
    )
);

mux#(26, 6, 37) exu_mux(
    .key(op_code),
    .default_o(37'b0),
    .out({data_out, flag, cyc}),
    .lut(
        {
            6'b000000, special_data, special_flag, special_cyc,
            // see special_mux
            6'b000001, regimm_data, regimm_flag, regimm_cyc,
            // see regimm_mux
            6'b001000, add_out, 1'b0, 4'd0,
            // addi
            6'b001001, add_out, 1'b0, 4'd0,
            // addiu
            6'b001100, and_out, 1'b0, 4'd0,
            // andi
            6'b001111, add_out, 1'b0, 4'd0,
            // aui
            6'b000100, add_out, eq, 4'd0,
            // beq
            6'b000111, add_out, gtz, 4'd0,
            // bgtz
            6'b000110, add_out, lez, 4'd0,
            // blez
            6'b000101, add_out, ne, 4'd0,
            // bne
            6'b000010, add_out, 1'b1, 4'd0,
            // j
            6'b000011, pcp8, 1'b1, 4'd0,
            // jal
            6'b100000, add_out, 1'b0, 4'd0,
            // lb
            6'b100100, add_out, 1'b0, 4'd0,
            // lbu
            6'b100001, add_out, 1'b0, 4'd0,
            // lh
            6'b100101, add_out, 1'b0, 4'd0,
            // lhu
            6'b110000, add_out, 1'b0, 4'd0,
            // ll
            6'b100011, add_out, 1'b0, 4'd0,
            // lw
            6'b001101, or_out, 1'b0, 4'd0,
            // ori
            6'b101000, add_out, 1'b0, 4'd0,
            // sb
            6'b101001, add_out, 1'b0, 4'd0,
            // sh
            6'b001010, slt_out, 1'b0, 4'd0,
            // slti
            6'b001011, sltu_out, 1'b0, 4'd0,
            // sltiu
            6'b101011, add_out, 1'b0, 4'd0,
            // sw
            6'b001110, xor_out, 1'b0, 4'd0,
            // xori
            6'b011100, mul_out, 1'b0, 4'd8
            // mul
        }
    )
);

wire[14:0] inst_unused = {inst[25:21], inst[15:6]};

endmodule
