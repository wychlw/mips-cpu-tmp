module memu (
    input wire clk,
    input wire rst,
    
    // to ex
    input wire[31:0] ex_data_in,
    input wire mem_req,
    output wire mem_res,

    // to pipe
    input wire mem_r_in,
    input wire mem_w_in,
    input wire[4:0] pipe_wb_addr_in,
    input wire[31:0] inst_in,
    input wire[31:0] reg_data_in,

    // to bus
    output wire bus_req,
    output wire[31:0] bus_addr,
    output wire bus_rw,
    input wire[31:0] bus_data_in,
    output wire[31:0] bus_data_out,
    input wire bus_res,
    output wire[1:0] len,   // 00 for 0byte, 01 for 1byte, 10 for 2byte, 11 for 4byte

    // to reg
    output wire reg_en,
    output wire[31:0] wb_data,
    output wire[4:0] wb_addr,
    output wire mem_stage0
);

wire[31:0] ex_data;
dreg#(32, 32'b0) ex_data_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(ex_data_in),
    .dout(ex_data)
);

wire mem_r;
dreg#(1, 1'b0) memr_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(mem_r_in),
    .dout(mem_r)
);

wire mem_w;
dreg#(1, 1'b0) memw_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(mem_w_in),
    .dout(mem_w)
);

wire[4:0] pipe_wb_addr;
dreg#(5, 5'b0) pipe_wb_addr_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(pipe_wb_addr_in),
    .dout(pipe_wb_addr)
);

wire[31:0] inst;
dreg#(32, 32'b0) inst_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(inst_in),
    .dout(inst)
);

wire[31:0] reg_data;
dreg#(32, 32'b0) reg_data_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(reg_data_in),
    .dout(reg_data)
);

wire[31:0] bus_data;
dreg#(32, 32'b0) bus_data_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[1]),
    .din(bus_data_in),
    .dout(bus_data)
);

wire mem_oper;
wire[5:0] op_code;

/**
 * stages:
 *      stage0: ; waiting for mem_req
 *      stage1: recive mem_req; send mem_res
 *          stage1.1: if mem_r/mem_w: send bus_req, send bus_addr, send bus_rw
 *          stage1.2: goto stage2
 *      stage2: recive bus_res/no need rw mem; send en, send wb_data, send wb_data, goto stage0
 * 
 */
wire[2:0] stage;
wire nxt_stage;

assign nxt_stage = (stage[0] & mem_req) |
                   (stage[1] & (~mem_oper | bus_res)) |
                   (stage[2]);

circulate_reg#(3, 3'b001) stage_reg(
    .clk(clk),
    .rst(rst),
    .en(nxt_stage),
    .dout(stage)
);

assign mem_stage0 = stage[0];

assign mem_res = stage[1];

assign op_code = inst[31:26];

assign mem_oper = mem_r | mem_w;
assign mem_res = stage[1];
assign bus_req = stage[1] & mem_oper;
assign bus_addr = ex_data;
assign bus_rw = mem_w;
assign reg_en = stage[2];
assign wb_addr = pipe_wb_addr;
assign bus_data_out = mem_w ? reg_data : 32'b0;

wire[1:0] r_len;
mux#(6, 6, 34) wb_data_mux(
    .key(op_code),
    .default_o({ex_data, 2'b00}),
    .out({wb_data, r_len}),
    .lut(
        {
            6'b100000, {{24{bus_data[7]}}, bus_data[7:0]}, 2'b01,
            // lb
            6'b100100, {24'b0, bus_data[7:0]}, 2'b01,
            // lbu
            6'b100001, {{16{bus_data[15]}}, bus_data[15:0]}, 2'b10,
            // lh
            6'b100101, {16'b0, bus_data[15:0]}, 2'b10,
            // lhu
            6'b110000, bus_data, 2'b11,
            // ll
            6'b100011, bus_data, 2'b11
            // lw
        }
    )
);

assign len = &(op_code~^6'b101000) ? 2'b01 : // sb
             &(op_code~^6'b101001) ? 2'b10 : // sh
             &(op_code~^6'b101011) ? 2'b11 : // sw
             r_len;

wire[25:0] inst_unused = inst[25:0];

endmodule
