module cu (
    input wire clk,
    input wire rst,

    // to npc
    output wire npc_req,
    output wire jmp,
    input wire[31:0] pc_in,
    input wire pc_ready,

    // to reg forward
    output wire[4:0] reg0_addr,
    input wire[31:0] reg0_in,
    input wire reg0_valid,
    output wire[4:0] reg1_addr,
    input wire[31:0] reg1_in,
    input wire reg1_valid,

    // to bus
    output wire inst_req,
    output wire[31:0] inst_addr,
    input wire inst_res,
    input wire[31:0] inst_in,

    // to ex
    output wire ex_req,
    output wire[31:0] data0,
    output wire[31:0] data1,
    input wire ex_res,
    output wire[31:0] cmp_data0,
    output wire[31:0] cmp_data1,

    // to pipe
    // for the heck why I don't just throw the entire inst to next stage?
    output wire[31:0] inst_out,
    output wire[31:0] pc_out,
    output wire memr,
    output wire memw,
    output wire[4:0] wb_addr,
    output wire[31:0] reg1_data

);

wire[31:0] pc;

dreg#(32, 32'b0) pc_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[0]),
    .din(pc_in),
    .dout(pc)
);

wire[31:0] reg0;
dreg#(32, 32'b0) reg0_reg(
    .clk(clk),
    .rst(rst),
    .en(reg0_valid & stage[2]),
    .din(reg0_in),
    .dout(reg0)
);

wire[31:0] reg1;
dreg#(32, 32'b0) reg1_reg(
    .clk(clk),
    .rst(rst),
    .en(reg1_valid & stage[2]),
    .din(reg1_in),
    .dout(reg1)
);

wire[31:0] inst;
dreg#(32, 32'b0) inst_reg(
    .clk(clk),
    .rst(rst),
    .en(stage[1]),
    .din(inst_in),
    .dout(inst)
);

/**
 * stages:
 *      stage0: recive ex_res; waiting for pc_ready
 *      stage1: recive pc_ready; send inst_req, send inst_addr, (reset pc_ready signal)
 *      stage2: recive inst_res; prepair datas (wait for reg valid)
 *      stage3: send ex_req, cal all the sig
 *
 */
wire[3:0] stage;
wire nxt_stage;

assign nxt_stage = (stage[0] & pc_ready) |
                   (stage[1] & inst_res) |
                   (stage[2] & reg0_valid & reg1_valid) |
                   (stage[3] & ex_res);

circulate_reg#(4, 4'b0001) stage_reg(
    .clk(clk),
    .rst(rst),
    .en(nxt_stage),
    .dout(stage)
);

assign inst_req = stage[1];
assign ex_req = stage[3];
assign npc_req = stage[3];
assign inst_addr = pc;
assign reg1_data = reg1;
assign cmp_data0 = reg0;
assign cmp_data1 = reg1;
assign inst_out = inst;
assign pc_out = pc;

wire[31:0] imm_iz;   // zero extended
wire[31:0] imm_is;   // signed extended
wire[31:0] imm_ij;   // shift for jmp offset
wire[31:0] imm_j;
wire[31:0] imm_sh;
wire[4:0] rs, rt, rd;
wire[4:0] zreg = 5'b0;

wire[5:0] op_code;

assign imm_iz = {16'b0, inst[15:0]};
assign imm_is = {{16{inst[15]}}, inst[15:0]};
assign imm_ij = {{14{inst[15]}}, inst[15:0], 2'b00} + 4;
assign imm_sh = {27'b0, inst[10:6]};
assign imm_j = {pc[31:28], inst[25:0], 2'b0};

assign op_code = inst[31:26];

assign rs = inst[25:21];
assign rt = inst[20:16];
assign rd = inst[15:11];

// here to handel some special inst
wire[31:0] aui_imm = {inst[15:0], 16'b0};

// NOTICE: no release6 inst, no removed inst

// special mux
wire[5:0] funt_code;
assign funt_code = inst[5:0];

wire[4:0] special_rs, special_rt, special_rd;
wire[31:0] special_data0, special_data1;
wire special_jmp;
wire special_memr;
wire special_memw;

mux#(18, 6, 82) special_mux(
    .key(funt_code),
    .default_o(82'b0),
    .out({special_rs, special_rt, special_rd, special_data0, special_data1, special_jmp, special_memr, special_memw}),
    .lut(
        {
            6'b100000, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // add
            6'b100001, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // addu
            6'b100100, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // and
            6'b001000, rs, zreg, zreg, reg0, reg1, 1'b1, 1'b0, 1'b0,
            // jr
            6'b001001, rs, zreg, rd, reg0, reg1, 1'b1, 1'b0, 1'b0,
            // jalr
            6'b100111, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // nor
            6'b100101, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // or
            6'b000000, rt, zreg, rd, reg0, imm_sh, 1'b0, 1'b0, 1'b0,
            // sll
            6'b000100, rt, rs, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // sllv
            6'b101010, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // slt
            6'b101011, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // sltu
            6'b000011, rt, zreg, rd, reg0, imm_sh, 1'b0, 1'b0, 1'b0,
            // sra
            6'b000111, rt, rs, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // srav
            6'b000010, rt, zreg, rd, reg0, imm_sh, 1'b0, 1'b0, 1'b0,
            // srl
            6'b000110, rt, rs, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // srlv
            6'b100010, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // sub
            6'b100011, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0,
            // subu
            6'b100110, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0
            // xor
        }
    )
);

// regimm mux
wire[4:0] regimm_code;
assign regimm_code = inst[20:16];

wire[4:0] regimm_rs, regimm_rt, regimm_rd;
wire[31:0] regimm_data0, regimm_data1;
wire regimm_jmp;
wire regimm_memr;
wire regimm_memw;

mux#(2, 5, 82) regimm_mux(
    .key(regimm_code),
    .default_o(82'b0),
    .out({regimm_rs, regimm_rt, regimm_rd, regimm_data0, regimm_data1, regimm_jmp, regimm_memr, regimm_memw}),
    .lut(
        {
            5'b00001, rs, zreg, zreg, pc, imm_ij, 1'b1, 1'b0, 1'b0,
            // bgez
            5'b00000, rs, zreg, zreg, pc, imm_ij, 1'b1, 1'b0, 1'b0
            // bltz
        }
    )
);

// TODO inst: all inst with exception
// TODO inst: all inst with float

// big total mux, if add a inst, just add at here
mux#(27, 6, 82) cu_mux(
    .key(op_code),
    .default_o(82'b0),
    .out({reg0_addr, reg1_addr, wb_addr, data0, data1, jmp, memr, memw}),
    .lut(
        {
            6'b000000, special_rs, special_rt, special_rd, special_data0, special_data1, special_jmp, special_memr, special_memw, 
            // special: add addu and jalr nor or sll sllv 
            //          slt sltu sra srav srl srlv sub subu xor
            6'b001000, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b0, 1'b0,
            // addi
            6'b001001, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b0, 1'b0,
            // addiu
            6'b001100, rs, zreg, rt, reg0, imm_iz, 1'b0, 1'b0, 1'b0,
            // andi
            6'b001111, rs, zreg, rt, reg0, aui_imm, 1'b0, 1'b0, 1'b0,
            // aui
            6'b000100, rs, rt, zreg, pc, imm_ij, 1'b1, 1'b0, 1'b0,
            // beq
            6'b000001, regimm_rs, regimm_rt, regimm_rd, regimm_data0, regimm_data1, regimm_jmp, regimm_memr, regimm_memw,
            // regimm: bgez blez
            6'b000111, rs, zreg, zreg, pc, imm_ij, 1'b1, 1'b0, 1'b0,
            // bgtz
            6'b000110, rs, zreg, zreg, pc, imm_ij, 1'b1, 1'b0, 1'b0,
            // blez
            6'b000101, rs, rt, zreg, pc, imm_ij, 1'b1, 1'b0, 1'b0,
            // bne
            6'b011111, rs, zreg, rt, reg0, imm_iz, 1'b1, 1'b0, 1'b0,
            // TODO
            // special3: ext, ins, *lbe, *lbue, *lhe, *lhue, *ll, *lle,
            // *lwe *lwle *sbe *sc *sce *seb *seh *she *swe
            6'b000010, zreg, zreg, zreg, imm_j, reg0, 1'b1, 1'b0, 1'b0,
            // j
            6'b000011, zreg, zreg, 5'b11111, imm_j, reg0, 1'b1, 1'b0, 1'b0,
            // jal
            6'b100000, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b1, 1'b0,
            // lb
            6'b100100, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b1, 1'b0,
            // lbu
            6'b100001, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b1, 1'b0,
            // lh
            6'b100101, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b1, 1'b0,
            // lhu
            6'b110000, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b1, 1'b0,
            // ll
            6'b100011, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b1, 1'b0,
            // lw
            6'b001101, rs, zreg, rt, reg0, imm_iz, 1'b0, 1'b0, 1'b0,
            // ori
            6'b101000, rs, rt, zreg, reg0, imm_is, 1'b0, 1'b0, 1'b1,
            // sb
            6'b101001, rs, rt, zreg, reg0, imm_is, 1'b0, 1'b0, 1'b1,
            // sh
            6'b001010, rs, zreg, rt, reg0, imm_is, 1'b0, 1'b0, 1'b0,
            // slti
            6'b001011, rs, zreg, rt, reg0, imm_iz, 1'b0, 1'b0, 1'b0,
            // sltiu
            6'b101011, rs, rt, zreg, reg0, imm_is, 1'b0, 1'b0, 1'b1,
            // sw
            6'b001110, rs, zreg, rt, reg0, imm_iz, 1'b0, 1'b0, 1'b0,
            // xori
            6'b011100, rs, rt, rd, reg0, reg1, 1'b0, 1'b0, 1'b0
            // mul
        }
    )
);

endmodule
