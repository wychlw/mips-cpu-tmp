module core_top (
    input wire clk,
    input wire rst,
    
    output wire inst_req,
    input wire inst_res,
    output wire[31:0] inst_addr,
    input wire[31:0] fetch_inst,

    output wire bus_req,
    input wire bus_res,
    output wire bus_rw,
    input wire[31:0] bus_data_in,
    output wire[31:0] bus_data_out,
    output wire[31:0] bus_addr,
    output wire[1:0] len
);

// npc to exu
wire exnpc_req;
wire npc_flag;
wire[31:0] jaddr;

// npc to cu
wire cunpc_req;
wire npc_jmp;
wire[31:0] npc_pc;
wire pc_ready;

// reg_forward to regs
wire[4:0] forward_reg0_addr;
wire[31:0] forward_reg0;
wire[4:0] forward_reg1_addr;
wire[31:0] forward_reg1;

// cu to reg_forward
wire[4:0] cu_reg0_addr;
wire[31:0] cu_reg0;
wire cu_reg0_valid;
wire[4:0] cu_reg1_addr;
wire[31:0] cu_reg1;
wire cu_reg1_valid;

// cu to ex
wire ex_req;
wire[31:0] ex_data0;
wire[31:0] ex_data1;
wire ex_res;
wire[31:0] cmp_data0;
wire[31:0] cmp_data1;

// cu to pipe
wire[31:0] cu_inst;
wire[31:0] cu_pc;
wire cu_memr;
wire cu_memw;
wire[4:0] cu_wb_addr;
wire[31:0] cu_reg1_data;

// exu to mem
wire mem_req;
wire[31:0] mem_data;
wire mem_res;

// exu to pipe
wire exu_stage0;

// exu level pipe
wire[31:0] exu_inst;
wire exu_memr;
wire exu_memw;
wire[4:0] exu_wb_addr;
wire[31:0] exu_reg1;

// mem to reg
wire mem_wb_en;
wire[4:0] mem_wb_addr;
wire[31:0] mem_wb_data;
wire mem_stage0;

npc npc_comp(
    .clk(clk),
    .rst(rst),
    .ex_req(exnpc_req),
    .flag_in(npc_flag),
    .jaddr_in(jaddr),
    .cu_req(cunpc_req),
    .jmp(npc_jmp),
    .pc(npc_pc),
    .pc_ready(pc_ready)
);

regs regs_comp(
    .clk(clk),
    .rst(rst),
    .s(forward_reg0_addr),
    .r(forward_reg1_addr),
    .w(mem_wb_addr),
    .w_en(mem_wb_en),
    .s_d(forward_reg0),
    .r_d(forward_reg1),
    .w_d(mem_wb_data)
);

reg_forward reg_forward_comp(
    .ex_wbaddr(exu_wb_addr),
    .ex_out(mem_data),
    .ex_stage0(exu_stage0),
    .mem_req(mem_req),
    .mem_wbaddr(mem_wb_addr),
    .mem_out(mem_wb_data),
    .memwb_en(mem_wb_en),
    .mem_stage0(mem_stage0),
    .regs_reg0_addr(forward_reg0_addr),
    .regs_reg0(forward_reg0),
    .regs_reg1_addr(forward_reg1_addr),
    .regs_reg1(forward_reg1),
    .reg0_addr(cu_reg0_addr),
    .reg0(cu_reg0),
    .reg0_valid(cu_reg0_valid),
    .reg1_addr(cu_reg1_addr),
    .reg1(cu_reg1),
    .reg1_valid(cu_reg1_valid)
);

cu cu_comp(
    .clk(clk),
    .rst(rst),
    .npc_req(cunpc_req),
    .jmp(npc_jmp),
    .pc_in(npc_pc),
    .pc_ready(pc_ready),
    .reg0_addr(cu_reg0_addr),
    .reg0_in(cu_reg0),
    .reg0_valid(cu_reg0_valid),
    .reg1_addr(cu_reg1_addr),
    .reg1_in(cu_reg1),
    .reg1_valid(cu_reg1_valid),
    .inst_req(inst_req),
    .inst_addr(inst_addr),
    .inst_res(inst_res),
    .inst_in(fetch_inst),
    .ex_req(ex_req),
    .data0(ex_data0),
    .data1(ex_data1),
    .ex_res(ex_res),
    .cmp_data0(cmp_data0),
    .cmp_data1(cmp_data1),
    .inst_out(cu_inst),
    .pc_out(cu_pc),
    .memr(cu_memr),
    .memw(cu_memw),
    .wb_addr(cu_wb_addr),
    .reg1_data(cu_reg1_data)
);

exu exu_comp(
    .clk(clk),
    .rst(rst),
    .ex_req(ex_req),
    .data0_in(ex_data0),
    .data1_in(ex_data1),
    .inst_in(cu_inst),
    .pc_in(cu_pc),
    .ex_res(ex_res),
    .cmp_data0_in(cmp_data0),
    .cmp_data1_in(cmp_data1),
    .npc_req(exnpc_req),
    .flag(npc_flag),
    .jaddr(jaddr),
    .mem_req(mem_req),
    .data_out(mem_data),
    .mem_res(mem_res),
    .stage0(exu_stage0)
);

dreg#(71, 71'b0) exu_pipe(
    .clk(clk),
    .rst(rst),
    .en(exu_stage0),
    .din({cu_inst, cu_memr, cu_memw, cu_wb_addr, cu_reg1_data}),
    .dout({exu_inst, exu_memr, exu_memw, exu_wb_addr, exu_reg1})
);

memu memu_comp(
    .clk(clk),
    .rst(rst),
    .ex_data_in(mem_data),
    .mem_req(mem_req),
    .mem_res(mem_res),
    .mem_r_in(exu_memr),
    .mem_w_in(exu_memw),
    .pipe_wb_addr_in(exu_wb_addr),
    .inst_in(exu_inst),
    .reg_data_in(exu_reg1),
    .bus_req(bus_req),
    .bus_addr(bus_addr),
    .bus_rw(bus_rw),
    .bus_data_in(bus_data_in),
    .bus_data_out(bus_data_out),
    .bus_res(bus_res),
    .len(len),
    .reg_en(mem_wb_en),
    .wb_data(mem_wb_data),
    .wb_addr(mem_wb_addr),
    .mem_stage0(mem_stage0)
);

endmodule
