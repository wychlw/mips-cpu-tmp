module uart_tx(
    input wire clk,
    input wire rst,

    output wire txd,
    input wire baud_clk,

    input wire[7:0] tx_data_in,
    input wire tx_start,
    output wire tx_busy
);

// gen baud: 11.0592M / 1152
wire[10:0] div_in, div_out;
wire baud;
dreg#(11, 11'd0) div_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(1'b1),
    .din(div_in),
    .dout(div_out)
);

assign div_in = div_out == 1151 ? 11'b0 : div_out + 1;
assign baud = div_out == 0;

wire nxt_stage;

wire[11:0] state;

wire tx_start_in, tx_start_out;
dreg#(1, 1'b0) tx_start_reg
(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(tx_start_in),
    .dout(tx_start_out)
);
wire[7:0] tx_data;
dreg#(8, 8'd1) tx_reg(
    .clk(clk),
    .rst(rst),
    .en(tx_start),
    .din(tx_data_in),
    .dout(tx_data)
);
assign tx_start_in = tx_start ? 1'b1 :
                     ~state[0] ? 1'b0 :
                     tx_start_out;

assign nxt_stage = (state[0] & tx_start_out & baud) |
                   (~state[0] & baud);
assign tx_busy = ~state[0] | tx_start_out;
circulate_reg#(12, 12'b00000000001) state_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(nxt_stage),
    .dout(state)
);

wire[11:0] txd_send = {2'b11 ,tx_data,1'b0, 1'b1};
assign txd = |(txd_send & state);

endmodule
