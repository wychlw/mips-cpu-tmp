/**
 *  Memory mapping detail:
 *      0xbfd003fc           : UART STAT
 *          rx_ready, tx_ready
 *      0xbfd003f8           : UART DATA
 */

module iommu (
    input wire clk,
    input wire rst,
    
    // mmu input
    input wire[31:0] addr,
    input wire[31:0] data_in,
    output wire[31:0] data_out,
    input wire rw,
    input wire[3:0] be,
    input wire req,
    output wire res,

    // uart
    input wire rx_rdy,
    output wire rx_clr,
    input wire[7:0] rx_data,

    output wire tx_start,
    input wire tx_busy,
    output wire[7:0] tx_data
);

wire is_uart_stat;
wire is_uart_data;
wire is_uart_rx;
wire is_uart_tx;

assign is_uart_stat = &(addr ~^ 32'hbfd003fc) & req;
assign is_uart_data = &(addr ~^ 32'hbfd003f8) & req;
assign is_uart_rx = is_uart_data & ~rw;
assign is_uart_tx = is_uart_data & rw;

// for now, we assume every IO reg use one tick
wire res_tmp;
assign res = res_tmp & req;
delay_reg#(1) res_delay(
    .clk(clk),
    .rst(rst | ~req),
    .din(req),
    .dout(res_tmp)
);

wire[31:0] data_out_raw;
dreg#(32) data_out_reg(
    .clk(clk),
    .rst(rst),
    .en(req & ~res),
    .din(data_out_raw),
    .dout(data_out)
);

// uart rx
assign rx_clr = (is_uart_rx & res) | rst;

// uart tx
dreg#(8) tx_data_reg(
    .clk(clk),
    .rst(rst),
    .en(is_uart_tx),
    .din(data_in[7:0]),
    .dout(tx_data)
);
assign tx_start = is_uart_tx;
// assign tx_start = 1'b1;

// out mux
assign data_out_raw = is_uart_stat ? {30'b0, rx_rdy, ~tx_busy} :
                      is_uart_rx ? {24'b0, rx_data} : 
                      32'b0;
endmodule //iommu
