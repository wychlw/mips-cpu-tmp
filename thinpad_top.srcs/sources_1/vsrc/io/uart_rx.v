module uart_rx(
    input wire clk,
    input wire rst,

    input wire rxd,
    input wire baud_clk,

    output wire[7:0] rx_data_out,
    input wire rx_rst,
    output wire rx_ready
);

wire[9:0] rx_data_reg_in, rx_data_reg_out;
dreg#(10, 10'b0) rx_data_reg(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(rx_data_reg_in),
    .dout(rx_data_reg_out)
);

assign rx_data_out = rx_data_reg_out[8:1];

// gen sample: 11.0592M / 1152 * 8 for over sampling
wire[7:0] sample_in, sample_out;
wire sample;
dreg#(8, 8'b0) sample_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(1'b1),
    .din(sample_in),
    .dout(sample_out)
);

assign sample_in = sample_out == 143 ? 8'b0 : sample_out + 1;
assign sample = (sample_out == 0);

// gen baud: 11.0592M / 1152
wire[10:0] div_in, div_out;
wire baud;
dreg#(11, 11'd0) div_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(1'b1),
    .din(div_in),
    .dout(div_out)
);

assign div_in = div_out == 1151 ? 11'b0 : div_out + 1;
assign baud = div_out == 0;


wire[2:0] os_in, os_out; // os: over sample
wire rx_data_in;
dreg#(3, 3'b111) os_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(1'b1),
    .din(os_in),
    .dout(os_out)
);

assign os_in = baud ? 3'b0 :
               sample & rxd ? os_out + 1 :
               os_out;

dreg#(1, 1'b1) rx_data_in_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(1'b1),
    .din(baud ? os_out >= 4 : rx_data_in),
    .dout(rx_data_in)
);

wire nxt_stage;

wire[8:0] state;

assign nxt_stage = (state[0] & ~rx_data_in & baud) |
                   (~state[0] & baud);
circulate_reg#(9, 9'b000000001) state_reg(
    .clk(baud_clk),
    .rst(rst),
    .en(nxt_stage),
    .dout(state)
);

assign rx_data_reg_in = rx_data_in ? rx_data_reg_out | state : rx_data_reg_out & ~state;

wire rx_ready_in;
dreg#(1, 1'b0) rx_ready_reg(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(rx_ready_in),
    .dout(rx_ready)
);

assign rx_ready_in = rx_rst ? 1'b0 :
                     rx_ready | state[8];

endmodule
