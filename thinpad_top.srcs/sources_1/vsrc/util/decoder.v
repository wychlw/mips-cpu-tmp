module decoder #(
    DATA_LEN = 1
) (
    input wire en,
    input wire[DATA_LEN - 1 : 0] x,
    output wire[2**DATA_LEN - 1 : 0] y
);
wire[2**DATA_LEN - 1 : 0] res;
genvar i;
generate
    for (i = 0; i < 2**DATA_LEN; i = i + 1)
    begin: sel
        assign res[i] = &(x~^i) ? 1'b1 : 1'b0;
    end
endgenerate

assign y = en ? res : 0;

endmodule
