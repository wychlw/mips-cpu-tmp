module delay_reg #(
    DELAY = 1
) (
    input wire clk,
    input wire rst,
    input wire din,
    output wire dout
);
wire[DELAY - 1 : 0] shift_out;

shift_reg#(DELAY) delayed(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(din),
    .dout(shift_out)
);

assign dout = shift_out[DELAY - 1];

endmodule
