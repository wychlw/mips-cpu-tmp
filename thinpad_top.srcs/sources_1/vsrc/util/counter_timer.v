module counter_timer #(
    LEN = 8
) (
    input wire clk,
    input wire rst,
    input wire[LEN - 1 : 0] cyc,
    input wire start,
    output wire fin
);

wire[LEN - 1 : 0] reg_out;
assign fin = ~start & ~(|reg_out);

wire[LEN - 1 : 0] reg_in;
assign reg_in = start ? cyc : 
                fin ? reg_out - 1 :
                0;


dreg#(LEN ,0) counter_reg(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(reg_in),
    .dout(reg_out)
);

endmodule
