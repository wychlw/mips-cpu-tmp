module sr_reg (
    input wire clk,
    input wire rst,
    input wire s,
    input wire r,
    output wire out
);

dreg#(1,1'b0) sr_dreg(
    .clk(clk),
    .rst(rst | r),
    .en(s),
    .din(1'b1),
    .dout(out)
);

endmodule
