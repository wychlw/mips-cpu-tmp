module mux #(
    LUT_LEN = 2,
    KEY_LEN = 1,
    DATA_LEN = 1
) (
    input wire[KEY_LEN - 1 : 0] key,
    input wire[DATA_LEN - 1 : 0] default_o,
    input wire[LUT_LEN * (KEY_LEN + DATA_LEN) - 1 : 0] lut,
    output wire[DATA_LEN - 1 : 0] out
);

// seprate lut table
wire[KEY_LEN - 1 : 0] keys[LUT_LEN - 1 : 0];
wire[DATA_LEN - 1 : 0] datas[LUT_LEN - 1 : 0];
genvar i;
generate
    for (i = 0; i < LUT_LEN; i = i + 1)
    begin: decode_lut
        assign keys[i] =
            lut[(KEY_LEN + DATA_LEN) * (i + 1) - 1 : (KEY_LEN + DATA_LEN) * i + DATA_LEN];
        assign datas[i] =
            lut[(KEY_LEN + DATA_LEN) * i + DATA_LEN - 1 : (KEY_LEN + DATA_LEN) * i];
    end
endgenerate

// decoder
wire[LUT_LEN - 1 : 0] choose;
generate
    for (i = 0; i < LUT_LEN; i = i + 1)
    begin: choose_key
        assign choose[i] = &(key ~^ keys[i]);
    end
endgenerate

// is default
wire has;
assign has = |choose;

// decoder choose AND datas
wire[DATA_LEN - 1 : 0] decoder_and_out[LUT_LEN -1 : 0];
generate
    for (i = 0; i < LUT_LEN; i = i + 1)
    begin: choose_data
        assign decoder_and_out[i] = choose[i] ? datas[i] : 0;
    end
endgenerate

// now to OR every line together.
// to achieve this, first transform, then OR every colume

wire[LUT_LEN -1 : 0] decoder_and_out_T[DATA_LEN - 1 : 0];
genvar j;
generate
    for (i = 0; i < LUT_LEN; i = i + 1)
    begin
        for (j = 0; j < DATA_LEN; j = j + 1)
        begin
            assign decoder_and_out_T[j][i] = decoder_and_out[i][j];
        end
    end
endgenerate

// get the res with no default
wire[DATA_LEN - 1 : 0] res;
generate
    for (i = 0; i < DATA_LEN; i = i + 1)
    begin
        assign res[i] = |decoder_and_out_T[i];
    end
endgenerate

// now add the default
assign out = has ? res : default_o;

endmodule
