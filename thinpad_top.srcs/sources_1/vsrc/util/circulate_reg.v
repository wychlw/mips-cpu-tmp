module circulate_reg #(
    LEN = 8,
    RESET_VAL = 8'b00000001
) (
    input wire clk,
    input wire rst,
    input wire en,
    output wire[LEN - 1 : 0] dout
);
wire[LEN : 0] reg_out /*verilator split_var*/;

genvar i;
assign reg_out[0] = reg_out[LEN];
generate
    for (i = 1; i <= LEN; i = i + 1)
    begin: shift_d_regs
        dreg#(1, RESET_VAL[i-1]) reg_t(
            .clk(clk),
            .rst(rst),
            .en(en),
            .din(reg_out[i-1]),
            .dout(reg_out[i])
        );
    end
endgenerate

assign dout = reg_out[LEN : 1];

endmodule
