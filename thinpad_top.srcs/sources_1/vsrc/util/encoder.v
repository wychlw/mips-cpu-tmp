module encoder #(
    DATA_LEN = 1
) (
    input wire en,
    input wire[2**DATA_LEN - 1 : 0] x,
    output wire[DATA_LEN - 1 : 0] y
);

wire[DATA_LEN - 1 : 0] res;
wire[2**DATA_LEN - 1 : 0] onehot;

genvar i;
assign onehot[2**DATA_LEN - 1] = x[2**DATA_LEN - 1];
generate
    for (i = 2**DATA_LEN - 2; i >= 0; i = i - 1)
    begin
        assign onehot[i] = x[i] & ~(|x[2**DATA_LEN - 1 : i + 1]);
    end
endgenerate

wire[2**DATA_LEN - 1 : 0] and_onehot[DATA_LEN - 1 : 0];
genvar j;
generate
    for (i = 0; i < 2**DATA_LEN; i = i + 1)
    begin
        for (j = 0; j < DATA_LEN; j = j + 1)
        begin
            assign and_onehot[j][i] = onehot[i] ? i[j] : 1'b0;
        end
    end
endgenerate

generate
    for (i = 0; i < DATA_LEN; i = i + 1)
    begin
        assign res[i] = |and_onehot[i];
    end
endgenerate

assign y = en ?  res : 0;

endmodule
