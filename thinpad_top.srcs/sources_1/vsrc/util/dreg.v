module dreg #(WIDTH = 1, RESET_VAL = 0) (
  input clk,
  input rst,
  input en,
  input [WIDTH-1:0] din,
  output reg [WIDTH-1:0] dout
);
  always @(posedge clk) begin
    if (rst) dout <= RESET_VAL;
    else if (en) dout <= din;
  end
endmodule
