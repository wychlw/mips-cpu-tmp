module shift_reg #(
    LEN = 8
) (
    input wire clk,
    input wire rst,
    input wire en,
    input wire din,
    output wire[LEN - 1 : 0] dout
);
wire[LEN : 0] reg_out /*verilator split_var*/;

genvar i;
assign reg_out[0] = din;
generate
    for (i = 1; i <= LEN; i = i + 1)
    begin: shift_d_regs
        dreg#(1, 0) reg_t(
            .clk(clk),
            .rst(rst),
            .en(en),
            .din(reg_out[i-1]),
            .dout(reg_out[i])
        );
    end
endgenerate

assign dout = reg_out[LEN : 1];

endmodule
