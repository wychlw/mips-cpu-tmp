module mul32 (
    input wire[31:0] a,
    input wire[31:0] b,
    output wire[31:0] out
);
wire sgn;
assign sgn = a[31] ^ b[31];

wire[32:0] b_ext;
assign b_ext = {b, 1'b0};
wire[31:0] pp0[15:0];
generate
    genvar i;
    for (i = 0; i < 16; i = i + 1)
    begin: pp0_gen
        booth_pp_gen #(
            .LENGTH(32)
        ) pp0_gen (
            .y(a),
            .x(b_ext[2 * i + 2 : 2 * i]),
            .out(pp0[i])
        );
    end
endgenerate

// pp0 - pp1, 16 - 11
wire[31:0] pp1_0, pp1_1;
compressor_32#(32) pp1_gen_1(
    .a(pp0[0][31:0]),
    .b({pp0[1][29:0], 2'b0}),
    .c({pp0[2][27:0], 4'b0}),
    .o1(pp1_0),
    .o2(pp1_1)
);
wire[25:0] pp1_2, pp1_3;
compressor_32#(26) pp1_gen_2(
    .a(pp0[3][25:0]),
    .b({pp0[4][23:0], 2'b0}),
    .c({pp0[5][21:0], 4'b0}),
    .o1(pp1_2),
    .o2(pp1_3)
);
wire[19:0] pp1_4, pp1_5;
compressor_32#(20) pp1_gen_3(
    .a(pp0[6][19:0]),
    .b({pp0[7][17:0], 2'b0}),
    .c({pp0[8][15:0], 4'b0}),
    .o1(pp1_4),
    .o2(pp1_5)
);
wire[13:0] pp1_6, pp1_7;
compressor_32#(14) pp1_gen_4(
    .a(pp0[9][13:0]),
    .b({pp0[10][11:0], 2'b0}),
    .c({pp0[11][9:0], 4'b0}),
    .o1(pp1_6),
    .o2(pp1_7)
);
wire[7:0] pp1_8, pp1_9;
compressor_32#(8) pp1_gen_5(
    .a(pp0[12][7:0]),
    .b({pp0[13][5:0], 2'b0}),
    .c({pp0[14][3:0], 4'b0}),
    .o1(pp1_8),
    .o2(pp1_9)
);
wire[1:0] pp1_10;
assign pp1_10 = pp0[15][1:0];

// pp1 - pp2, 11 - 8
wire[31:0] pp2_0, pp2_1;
compressor_32#(32) pp2_gen_1(
    .a(pp1_0[31:0]),
    .b(pp1_1[31:0]),
    .c({pp1_2[25:0], 6'b0}),
    .o1(pp2_0),
    .o2(pp2_1)
);
wire[25:0] pp2_2, pp2_3;
compressor_32#(26) pp2_gen_2(
    .a(pp1_3[25:0]),
    .b({pp1_4[19:0], 6'b0}),
    .c({pp1_5[19:0], 6'b0}),
    .o1(pp2_2),
    .o2(pp2_3)
);
wire[13:0] pp2_4, pp2_5;
compressor_32#(14) pp2_gen_3(
    .a(pp1_6[13:0]),
    .b(pp1_7[13:0]),
    .c({pp1_8[7:0], 6'b0}),
    .o1(pp2_4),
    .o2(pp2_5)
);
wire[7:0] pp2_6;
assign pp2_6 = pp1_9[7:0];
wire[1:0] pp2_7;
assign pp2_7 = pp1_10[1:0];

// pp2 - pp3, 8 - 6
wire[31:0] pp3_0, pp3_1;
compressor_32#(32) pp3_gen_1(
    .a(pp2_0[31:0]),
    .b(pp2_1[31:0]),
    .c({pp2_2[25:0], 6'b0}),
    .o1(pp3_0),
    .o2(pp3_1)
);
wire[25:0] pp3_2, pp3_3;
compressor_32#(26) pp3_gen_2(
    .a(pp2_3[25:0]),
    .b({pp2_4[13:0], 12'b0}),
    .c({pp2_5[13:0], 12'b0}),
    .o1(pp3_2),
    .o2(pp3_3)
);
wire[7:0] pp3_4;
assign pp3_4 = pp2_6[7:0];
wire[1:0] pp3_5;
assign pp3_5 = pp2_7[1:0];

// pp3 - pp4, 6 - 4
wire[31:0] pp4_0, pp4_1;
compressor_32#(32) pp4_gen_1(
    .a(pp3_0[31:0]),
    .b(pp3_1[31:0]),
    .c({pp3_2[25:0], 6'b0}),
    .o1(pp4_0),
    .o2(pp4_1)
);
wire[25:0] pp4_2, pp4_3;
compressor_32#(26) pp4_gen_2(
    .a(pp3_3[25:0]),
    .b({pp3_4[7:0], 18'b0}),
    .c({pp3_5[1:0], 24'b0}),
    .o1(pp4_2),
    .o2(pp4_3)
);

// pp4 - pp5, 4 - 3
wire[31:0] pp5_0, pp5_1;
compressor_32#(32) pp5_gen_1(
    .a(pp4_0[31:0]),
    .b(pp4_1[31:0]),
    .c({pp4_2[25:0], 6'b0}),
    .o1(pp5_0),
    .o2(pp5_1)
);
wire[25:0] pp5_2;
assign pp5_2 = pp4_3[25:0];

// pp5 - pp6, 3 - 2
wire[31:0] pp6_0, pp6_1;
compressor_32#(32) pp6_gen_1(
    .a(pp5_0[31:0]),
    .b(pp5_1[31:0]),
    .c({pp5_2[25:0], 6'b0}),
    .o1(pp6_0),
    .o2(pp6_1)
);

// pp6 - pp7, 2 - 1
wire[31:0] pp7_0;
wire co_unused, of_unused;
adder32 pp7_gen(
    .a(pp6_0[31:0]),
    .b(pp6_1[31:0]),
    .ci(1'b0),
    .s(pp7_0),
    .co(co_unused),
    .of(of_unused)
);

// final
assign out = {sgn, pp7_0[30:0]};

endmodule