module lshifter #(
    DATA_LEN = 32
) (
    input wire[DATA_LEN - 1 : 0] a,
    input wire[$clog2(DATA_LEN) - 1 : 0] b,
    output wire[DATA_LEN - 1 : 0] o
);
localparam SHIFT_LEN = $clog2(DATA_LEN);

wire[DATA_LEN - 1 : 0] tmp[SHIFT_LEN : 0] /*verilator split_var*/;

genvar i;
assign tmp[0] = a;
generate
    for (i = 0; i < SHIFT_LEN; i = i + 1)
    begin: bucket_lshift
        assign tmp[i + 1] = b[i] ? {tmp[i][(DATA_LEN - 2**i - 1) : 0], {(2**i){1'b0}} } : tmp[i];
    end
endgenerate

assign o = tmp[SHIFT_LEN];

endmodule
