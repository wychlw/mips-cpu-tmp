module adder32 (
    input wire[31:0] a,
    input wire[31:0] b,
    input wire ci,
    output wire[31:0] s,
    output wire co,
    output wire of
);
wire[32:0] carry;

wire unused_lbcla_p;
wire unused_lbcla_g;

assign carry[0] = ci;
mbcla adder32_mbcla(
    .a(a),
    .b(b),
    .c(ci),
    .o(carry[32:1]),
    .p(unused_lbcla_p),
    .g(unused_lbcla_g)
);

assign s = a ^ b ^ carry[31:0];
assign co = carry[32];
assign of = carry[32] ^ carry[31];

endmodule
