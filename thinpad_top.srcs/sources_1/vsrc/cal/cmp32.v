module cmp32 (
    input wire[31:0] a,
    input wire[31:0] b,
    output wire gt,
    output wire eq,
    output wire lt
);

wire[1:0] sa, sb;
wire eq_unused0, eq_unused1;

cmp16 comp0(
    .a(a[15:0]),
    .b(b[15:0]),
    .gt(sa[0]),
    .eq(eq_unused0),
    .lt(sb[0])
);
cmp16 comp1(
    .a(a[31:16]),
    .b(b[31:16]),
    .gt(sa[1]),
    .eq(eq_unused1),
    .lt(sb[1])
);
    
wire[1:0] fgt, feq, flt;
assign fgt = sa & ~sb;
assign feq = ~(sa ^ sb);
assign flt = ~sa & sb;

assign gt = fgt[1] | (feq[1] & fgt[0]);
assign eq = &feq;
assign lt = flt[1] | (feq[1] & flt[0]);

endmodule
