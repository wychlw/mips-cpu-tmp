module adder64 (
    input wire[63:0] a,
    input wire[63:0] b,
    input wire ci,
    output wire[63:0] s,
    output wire co,
    output wire of
);
wire[64:0] carry;

wire unused_lbcla_p;
wire unused_lbcla_g;

assign carry[0] = ci;
lbcla adder64_lbcla(
    .a(a),
    .b(b),
    .c(ci),
    .o(carry[64:1]),
    .p(unused_lbcla_p),
    .g(unused_lbcla_g)
);

assign s = a ^ b ^ carry[63:0];
assign co = carry[64];
assign of = carry[64] ^ carry[63];

endmodule
