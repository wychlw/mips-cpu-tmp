module cmp4 (
    input wire[3:0] a,
    input wire[3:0] b,
    output wire gt,
    output wire eq,
    output wire lt
);
    
wire[3:0] fgt, feq, flt;
assign fgt = a & ~b;
assign feq = ~(a ^ b);
assign flt = ~a & b;

assign gt = fgt[3] | (feq[3] & fgt[2]) | (feq[3] & feq[2] & fgt[1]) | (feq[3] & feq[2] & feq[1] & fgt[0]);
assign eq = &feq;
assign lt = flt[3] | (feq[3] & flt[2]) | (feq[3] & feq[2] & flt[1]) | (feq[3] & feq[2] & feq[1] & flt[0]);


endmodule
