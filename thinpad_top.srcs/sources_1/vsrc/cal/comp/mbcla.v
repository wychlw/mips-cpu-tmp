module mbcla (
    input wire[31:0] a,
    input wire[31:0] b,
    input wire c,
    output wire[31:0] o,
    output wire p,
    output wire g
);
wire c0, c15;
wire[1:0] p_i;
wire[1:0] g_i;

assign c0 = c;

bcla bcla0(
    .a(a[15:0]),
    .b(b[15:0]),
    .c(c0),
    .o(o[15:0]),
    .p(p_i[0]),
    .g(g_i[0])
);
assign c15 = g_i[0] | (p_i[0] & c0);

bcla bcla1(
    .a(a[31:16]),
    .b(b[31:16]),
    .c(c15),
    .o(o[31:16]),
    .p(p_i[1]),
    .g(g_i[1])
);

assign p = &p_i;
assign g = g_i[1] | (p_i[1] & g_i[0]);

endmodule
