module compressor_32 #(
    WIDTH = 32
) (
    input wire[WIDTH-1:0] a,
    input wire[WIDTH-1:0] b,
    input wire[WIDTH-1:0] c,
    output wire[WIDTH-1:0] o1,
    output wire[WIDTH-1:0] o2
);
wire[WIDTH-1:0] o2_tmp;
genvar i;
generate
    for (i = 0; i < WIDTH; i = i + 1) begin: gen
        assign o1[i] = a[i] ^ b[i] ^ c[i];
        assign o2_tmp[i] = (a[i] & b[i]) | (a[i] & c[i]) | (b[i] & c[i]);
    end
endgenerate

assign o2={o2_tmp[WIDTH-2:0], 1'b0};
endmodule
