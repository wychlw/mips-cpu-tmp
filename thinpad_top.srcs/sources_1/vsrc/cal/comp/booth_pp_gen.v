module booth_pp_gen #(
    LENGTH = 32
) (
    input wire[LENGTH - 1 : 0] y,
    input wire[2:0] x,
    output wire[LENGTH - 1:0] out
);

wire[LENGTH - 1 : 0] neg, y2, neg2;
assign neg = ~y + 1;
assign y2 = {y, 1'b0};
assign neg2 = {neg, 1'b0};

wire[LENGTH - 1 : 0] zeros;
assign zeros = {LENGTH{1'b0}};

mux#(8, 3, LENGTH) pp_mux(
    .key(x),
    .default_o(zeros),
    .out(out),
    .lut(
        {
            3'b000, zeros,
            3'b001, y,
            3'b010, y,
            3'b011, y2,
            3'b100, neg2,
            3'b101, neg,
            3'b110, neg,
            3'b111, zeros
        }
    )
);

endmodule
