module bcla (
    input wire[15:0] a,
    input wire[15:0] b,
    input wire c,
    output wire[15:0] o,
    output wire p,
    output wire g
);
wire c0, c3, c7, c11;
wire[3:0] p_i;
wire[3:0] g_i;

assign c0 = c;

cla cla0(
    .a(a[3:0]),
    .b(b[3:0]),
    .c(c0),
    .o(o[3:0]),
    .p(p_i[0]),
    .g(g_i[0])
);
assign c3 = g_i[0] | (p_i[0] & c0);

cla cla1(
    .a(a[7:4]),
    .b(b[7:4]),
    .c(c3),
    .o(o[7:4]),
    .p(p_i[1]),
    .g(g_i[1])
);
assign c7 = g_i[1] | (p_i[1] & g_i[0]) | (p_i[1] & p_i[0] & c0);

cla cla2(
    .a(a[11:8]),
    .b(b[11:8]),
    .c(c7),
    .o(o[11:8]),
    .p(p_i[2]),
    .g(g_i[2])
);
assign c11 = g_i[2] | (p_i[2] & g_i[1]) | (p_i[2] & p_i[1] & g_i[0]) | 
              (p_i[2] & p_i[1] & p_i[0] & c0);

cla cla3(
    .a(a[15:12]),
    .b(b[15:12]),
    .c(c11),
    .o(o[15:12]),
    .p(p_i[3]),
    .g(g_i[3])
);

assign p = &p_i;
assign g = g_i[3] | (p_i[3] & g_i[2]) | (p_i[3] & p_i[2] & g_i[1]) | 
           (p_i[3] & p_i[2] & p_i[1] & g_i[0]);

endmodule
