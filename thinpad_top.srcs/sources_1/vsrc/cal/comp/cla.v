module cla (
    input wire[3:0] a,
    input wire[3:0] b,
    input wire c,
    output wire[3:0] o,
    output wire p,
    output wire g
);
wire[3:0] p_i;
wire[3:0] g_i;

assign p_i = a | b;
assign g_i = a & b;

assign o[0] = g_i[0] | (p_i[0] & c);
assign o[1] = g_i[1] | (p_i[1] & g_i[0]) | (p_i[1] & p_i[0] & c);
assign o[2] = g_i[2] | (p_i[2] & g_i[1]) | (p_i[2] & p_i[1] & g_i[0]) | 
              (p_i[2] & p_i[1] & p_i[0] & c);
assign o[3] = g_i[3] | (p_i[3] & g_i[2]) | (p_i[3] & p_i[2] & g_i[1]) | 
              (p_i[3] & p_i[2] & p_i[1] & g_i[0]) | (p_i[3] & p_i[2] & p_i[1] & p_i[0] & c);

assign p = &p_i;
assign g = g_i[3] | (p_i[3] & g_i[2]) | (p_i[3] & p_i[2] & g_i[1]) | 
           (p_i[3] & p_i[2] & p_i[1] & g_i[0]);

endmodule
