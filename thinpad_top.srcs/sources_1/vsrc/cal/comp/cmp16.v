module cmp16 (
    input wire[15:0] a,
    input wire[15:0] b,
    output wire gt,
    output wire eq,
    output wire lt
);

wire[3:0] sa, sb;
wire eq_unused0, eq_unused1, eq_unused2, eq_unused3;

cmp4 comp0(
    .a(a[3:0]),
    .b(b[3:0]),
    .gt(sa[0]),
    .eq(eq_unused0),
    .lt(sb[0])
);
cmp4 comp1(
    .a(a[7:4]),
    .b(b[7:4]),
    .gt(sa[1]),
    .eq(eq_unused1),
    .lt(sb[1])
);
cmp4 comp2(
    .a(a[11:8]),
    .b(b[11:8]),
    .gt(sa[2]),
    .eq(eq_unused2),
    .lt(sb[2])
);
cmp4 comp3(
    .a(a[15:12]),
    .b(b[15:12]),
    .gt(sa[3]),
    .eq(eq_unused3),
    .lt(sb[3])
);

wire[3:0] fgt, feq, flt;
assign fgt = sa & ~sb;
assign feq = ~(sa ^ sb);
assign flt = ~sa & sb;

assign gt = fgt[3] | (feq[3] & fgt[2]) | (feq[3] & feq[2] & fgt[1]) | (feq[3] & feq[2] & feq[1] & fgt[0]);
assign eq = &feq;
assign lt = flt[3] | (feq[3] & flt[2]) | (feq[3] & feq[2] & flt[1]) | (feq[3] & feq[2] & feq[1] & flt[0]);

endmodule
