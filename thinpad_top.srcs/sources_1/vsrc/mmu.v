/**
 *  Memory mapping detail:
 *      0x80000000-0x8003ffff: Base SRAM
 *      0x80040000-0x8008ffff: Ext SRAM
 *      else: IOMMU
 */

module mmu (
    input wire clk,
    input wire rst,
    
    input wire[31:0] addr,
    input wire[31:0] data_in,
    output wire[31:0] data_out,
    input wire rw,
    input wire[1:0] wlen,
    input wire req,
    output wire res,

    inout wire[31:0] base_ram_data,
    output wire[19:0] base_ram_addr,
    output wire[3:0] base_ram_be_n,
    output wire base_ram_ce_n,
    output wire base_ram_oe_n,
    output wire base_ram_we_n,


    inout wire[31:0] ext_ram_data,
    output wire[19:0] ext_ram_addr,
    output wire[3:0] ext_ram_be_n,
    output wire ext_ram_ce_n,
    output wire ext_ram_oe_n,
    output wire ext_ram_we_n,

    output wire[31:0] iommu_addr,
    input wire[31:0] iommu_data_in,
    output wire[31:0] iommu_data_out,
    output wire iommu_rw,
    output wire[3:0] iommu_be,
    output wire iommu_req,
    input wire iommu_res
);
    wire is_ram;
    assign is_ram = &(addr[31:23] ~^ 9'b100000000);

    wire[31:0] data_out_raw;
    wire[31:0] ram_data_in;
    wire[31:0] ram_addr;
    wire ram_req, ram_res;

    // split data inout
    assign data_out_raw = rw ? 32'b0 :
                         is_ram ? ram_data_in :
                         iommu_data_in;
    assign res = (is_ram ? ram_res : iommu_res)&req;

    // detect unaligned access
    wire[3:0] be;
    wire unaligned_error_unused;
    mux#(7, 4, 37) be_mux(
        .key({wlen, addr[1:0]}),
        .default_o({4'b0000, 1'b1, 32'b0}),
        .out({be, unaligned_error_unused, data_out}),
        .lut(
            {
                2'b11, 2'b00, 4'b0000, 1'b0, data_out_raw[31:0],
                // 4byte access
                2'b10, 2'b00, 4'b1100, 1'b0, {16'b0, data_out_raw[15:0]},
                // 2byte access low
                2'b10, 2'b10, 4'b0011, 1'b0, {16'b0, data_out_raw[31:16]},
                // 2byte access high
                2'b01, 2'b00, 4'b1110, 1'b0, {24'b0, data_out_raw[7:0]},
                // 1byte access low low
                2'b01, 2'b01, 4'b1101, 1'b0, {24'b0, data_out_raw[15:8]},
                // 1byte access low high
                2'b01, 2'b10, 4'b1011, 1'b0, {24'b0, data_out_raw[23:16]},
                // 1byte access high low
                2'b01, 2'b11, 4'b0111, 1'b0, {24'b0, data_out_raw[31:24]}
                // 1byte access high high
            }
        )
    );

    // common sig between ram and data
    assign ram_addr = addr;
    assign iommu_addr = addr;
    assign iommu_rw = rw;
    
    // to ram
    wire ram_cs;
    assign ram_req = is_ram & req;
    assign ram_cs = ram_addr[22];

    wire ram_rw;
    assign ram_rw = rw;
    assign base_ram_addr = ram_addr[21:2];
    assign ext_ram_addr = ram_addr[21:2];
    assign base_ram_be_n = be;
    assign ext_ram_be_n = be;
    assign base_ram_ce_n = ~ram_req | ram_cs;
    assign ext_ram_ce_n = ~ram_req | ~ram_cs;
    assign base_ram_oe_n = base_ram_ce_n | ~ram_res | ram_rw;
    assign ext_ram_oe_n = ext_ram_ce_n | ~ram_res | ram_rw;
    assign base_ram_we_n = base_ram_ce_n | ~ram_res | ~ram_rw;
    assign ext_ram_we_n = ext_ram_ce_n | ~ram_res | ~ram_rw;
    assign ram_data_in = ~ext_ram_oe_n ? ext_ram_data :
                  ~base_ram_oe_n ? base_ram_data :
                  32'b0;
    assign base_ram_data = base_ram_we_n ? 32'bz : data_in;
    assign ext_ram_data = ext_ram_we_n ? 32'bz : data_in;
    delay_reg#(2) ram_res_delay(
        .clk(clk),
        .rst(rst | ~req),
        .din(ram_req),
        .dout(ram_res)
    );

    // to iommu
    assign iommu_req = ~is_ram & req;
    assign iommu_data_out = data_in;
    assign iommu_be = be;
endmodule
