module bus (
    input wire clk,
    input wire rst,

    input wire inst_req,
    output wire inst_res,
    input wire[31:0] inst_addr,
    output wire[31:0] fetch_inst,

    input wire bus_req,
    output wire bus_res,
    input wire bus_rw,
    input wire[31:0] bus_addr,
    output wire[31:0] bus_data_in,
    input wire[31:0] bus_data_out,
    input wire[1:0] len,

    output wire[31:0] addr,
    input wire[31:0] data_in,
    output wire[31:0] data_out,
    output wire rw,
    output wire[1:0] wlen,
    output wire req,
    input wire res
);

wire last_bus_req, last_inst_req;

dreg#(1,1'b0) last_bus_req_reg(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(bus_req),
    .dout(last_bus_req)
);

dreg#(1,1'b0) last_inst_req_reg(
    .clk(clk),
    .rst(rst),
    .en(1'b1),
    .din(inst_req),
    .dout(last_inst_req)
);

assign req = bus_req | inst_req;
assign addr = bus_req ? bus_addr : inst_addr;
assign rw = bus_req ? bus_rw : 1'b0;
assign wlen = bus_req ? len : 2'b11;
assign bus_res = bus_req & ~(last_inst_req & ~last_bus_req) ? res : 1'b0;
assign inst_res = inst_req & ~bus_req & ~last_bus_req ? res : 1'b0;
assign data_out = rw ? bus_data_out : 32'b0;
assign bus_data_in = bus_req & ~rw ? data_in : 32'b0;
assign fetch_inst = bus_req | rw ? 32'b0 : data_in;

endmodule //bus

